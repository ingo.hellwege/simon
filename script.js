/* class settings */
class Settings{
  /* constructor */
  constructor(){
    this.setupDefaults();
  }


  /* setup defaults */
  setupDefaults(){
    this.ovr=true;                     // game over status
    this.seq=new Array();              // color sequence (growing) computer
    this.plseq=new Array();            // color sequence (growing) from player
    this.tmr=750;                      // start with 0.75s timing
    this.tstp=50;                      // 50ms faster every level
    this.tmin=200;                     // fastest (minimum) speed
    this.nln=String.fromCharCode(10);  // line break
    this.btnaud='';                    // button audio
    this.clkaud='';                    // color button audio
    this.seqaud='';                    // sequence audio
    this.erraud='';                    // error audio
  }
}

/* class tools */
class Tools{
  /* constructor */
  constructor(set){
    this.SET=set;
  }


  /* add class to element */
  addClassToElementById(elmid,elmcls){
    var cls=this.getClassStringForElementById(elmid);
    if(cls.includes(elmcls)==false){
      var newcls=cls.concat(' '+elmcls);
      document.getElementById(elmid).className=newcls;
    }
  }

  /* remove class from element */
  removeClassFromElementById(elmid,elmcls){
    var cls=this.getClassStringForElementById(elmid);
    var newcls=cls.split(elmcls).join('');
    document.getElementById(elmid).className=newcls;
  }

  /* get the string with class names for element */
  getClassStringForElementById(elmid){
    return document.getElementById(elmid).className;
  }

  /* set inner html for element by id */
  setElementInnerHtmlById(elmid,str){
    document.getElementById(elmid).innerHTML=str;
  }

  /* do click on element by id */
  clickOnElementById(elmid){
    document.getElementById(elmid).click();
  }

  /* hide cover layer */
  hideCover(){
    this.addClassToElementById('cover1','cover1hide');
    this.addClassToElementById('cover2','cover2hide');
  }

  /* center game */
  centerWrapper(){
    var left=Math.floor((document.documentElement.clientWidth-document.getElementById('wrapper').offsetWidth)/2);
    document.getElementById('wrapper').style.marginLeft=left+'px';
    var top=Math.floor((document.documentElement.clientHeight-document.getElementById('wrapper').offsetHeight)/2);
    document.getElementById('wrapper').style.marginTop=top+'px';
  }

  /* get items for selector */
  getItemsForSelector(sel){
    return document.querySelectorAll(sel);
  }

  /* get a random number (0-3) */
  getRandomColorNumber(){
    return Math.floor(Math.random()*4);
  }
  
  /* remove item from array by key */
  removeItemFromArrayByKey(key,arr){
    arr.splice(key,1);
    return arr;
  }

  /* add item to array */
  addItemToArray(item,arr){
    arr.push(item);
    return arr;
  }

  /* play sound */
  playSound(snd){
    snd.cloneNode(true).play();
  }
}

/* class sound */
class Sound{
  /* constructor */
  constructor(){
    this.setupAudio();
  }


  /* setup game audio files */
  setupAudio(){
    this.btnaud=new Audio('button.wav');
    this.btnaud.volume=0.15;
    this.clkaud=new Audio('click.wav');
    this.clkaud.volume=0.75;
    this.seqaud=new Audio('beep.wav');
    this.seqaud.volume=0.15;
    this.erraud=new Audio('error.wav');
    this.erraud.volume=0.5;
  }
}

/* class game */
class Game{
  /* constructor */
  constructor(){
    // objects
    this.SET=new Settings();
    this.TLS=new Tools(this.SET);
    this.SND=new Sound();
    // listener
    addEventListener('click',this);
    addEventListener('keydown',this);
    addEventListener('resize',this);
  }


  /* lock player input */
  lockInput(){
    document.getElementById('game').setAttribute('attr-lck',1);
  }

  /* unlock player input */
  unlockInput(){
    document.getElementById('game').setAttribute('attr-lck',0);
  }

  /* is input locked */
  isLocked(){
    return (parseInt(document.getElementById('game').getAttribute('attr-lck'))==0)?false:true;
  }


  /* clear computer sequence */
  initComputerSequence(){
    this.SET.seq=new Array();
  }

  /* clear player sequence */
  initPlayerSequence(){
    this.SET.plseq=new Array();
  }

  /* add element to computer sequence */
  addElementToComputerSequence(){
    this.SET.seq.push(this.TLS.getRandomColorNumber());
  }

  /* add clicked element to player sequence */
  addElementToPlayerSequence(cnum){
    this.SET.plseq.push(parseInt(cnum));
  }

  /* blink button clicked by player */
  blinkClickedButton(cnum){
    var itemid='cbtn-'+cnum;
    // set button active
    this.TLS.addClassToElementById(itemid,'active');
    setTimeout(()=>{this.TLS.removeClassFromElementById(itemid,'active');},200);
  }


  /* display level */
  setLevel(){
    this.TLS.setElementInnerHtmlById('lvl',this.SET.seq.length);
  }

  /* reset level display */
  resetLevel(){
    console.log('reset');
    this.TLS.setElementInnerHtmlById('lvl','--');
    this.TLS.removeClassFromElementById('lvl','red');
  }


  /* play computeer sequence with timeout */
  playSequence(num){
    // computeer seqence step by step
    if(num<this.SET.seq.length){
      this.TLS.playSound(this.SND.seqaud);
      var itemid='cbtn-'+parseInt(this.SET.seq[num]);
      // set button active
      this.TLS.addClassToElementById(itemid,'active');
      // every step is making it 50ms faster
      var tms=parseInt(this.SET.tmr-(this.SET.tstp*this.SET.seq.length));
      if(tms<this.SET.tmin){tms=this.SET.tmin;}
      // set button inactive and continue
      setTimeout(()=>{
        this.TLS.removeClassFromElementById(itemid,'active');
        num++;
        setTimeout(()=>{this.playSequence(num);},200);
      },tms);
    // done - unlock player input
    }else{
      this.unlockInput();
    }
  }

  /* computer sequence */
  computerSequence(){
    this.lockInput();
    this.addElementToComputerSequence();
    this.setLevel();
    this.playSequence(0);
  }


  /* play game */
  playGame(){
    console.log('play');
    this.initComputerSequence();
    this.initPlayerSequence();
    this.SET.ovr=false;
    this.computerSequence();
  }

  /* check if game has ended */
  checkForEndGame(){
    console.log('check end game');
    // player sequence should be exactly like comuter sequence
    for(var x=0;x<this.SET.plseq.length;x++){
      if(this.SET.seq[x]!=this.SET.plseq[x]){this.endGame();}
    }
  }

  /* end of game */
  endGame(){
    console.log('game ended');
    this.TLS.playSound(this.SND.erraud);
    this.TLS.addClassToElementById('lvl','red');
    this.SET.ovr=true;
  }


  /* init game */
  init(){
    console.log('init');
    this.TLS.centerWrapper();
    this.resetLevel();
    this.TLS.hideCover();
  }


  /* handle listener events */
  handleEvent(event) {
    event.preventDefault();
    switch(event.type) {
      case 'click':
        this.leftClickEvent(event);
        break;
      case 'keydown':
        this.keyEvent(event);
        break;
      case 'resize':
        this.resizeEvent(event);
        break;
    }
  }

  /* mouse left click event */
  leftClickEvent(event){
    // clicked on a button
    if(event.target.matches('.button')){
      this.TLS.playSound(this.SND.btnaud);
      var action=event.target.getAttribute('attr-action');
      console.log(action);
      // what to do?
      if(action=='go'){
        this.resetLevel();
        setTimeout(()=>{this.playGame();},1000);
      }
    }
    // clicked on a color selector
    if(this.isLocked()==false&&(this.SET.ovr==false&&event.target.matches('.colbtn'))){
      this.TLS.playSound(this.SND.clkaud);
      var cnum=parseInt(event.target.getAttribute('id').replace('cbtn-',''));
      console.log('btn: '+cnum);
      this.blinkClickedButton(cnum);
      this.addElementToPlayerSequence(cnum);
      this.checkForEndGame();
      if(this.SET.ovr==false&&(this.SET.plseq.length==this.SET.seq.length)){
        this.initPlayerSequence();
        setTimeout(()=>{this.computerSequence();},1000);
      }
    }
  }

  /* keypress event */
  keyEvent(event){
    switch(event.code){
      // g(o)
      case 'KeyG':
        if(this.SET.ovr==true){this.TLS.clickOnElementById('go');}
        break;
      // r(eset)
      case 'KeyR':
        this.SET.ovr=true;
        this.resetLevel();
        break;
    }
  }

  /* resize event */
  resizeEvent(event){
    this.init();
  }
}



/* wait until document is fully loaded and ready to start */
let stateCheck=setInterval(()=>{
  if(document.readyState=='complete'){
    clearInterval(stateCheck);
    GME=new Game();
    GME.init();
    console.log('ready!');
  }
},250);
